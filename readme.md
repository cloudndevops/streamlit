1) Docker installation in amazon linux

sudo amazon-linux-extras install docker

From <https://docs.aws.amazon.com/AmazonECS/latest/developerguide/docker-basics.html> 
sudo yum install docker

From <https://docs.aws.amazon.com/AmazonECS/latest/developerguide/docker-basics.html> 

sudo service docker start

sudo usermod -a -G docker ec2-user


2) mkdir /app
chmod -R 777 /app

cd /app
git clone https://cloudndevops@bitbucket.org/cloudndevops/streamlit.git

cd streamlit/docker

3)  execute the script which will create 4 containers

nohup ./container.sh &


4)user full Docker commands 

To view docker images
--------------------
docker images


to view the containers:
----------------
docker ps


login into container
------------------
docker exec -it <containerid> sh

bash


ps -ef | grep python


kill -9 <pid>

root@6c3b593499bd:/app/colors# cat start.sh
#!/bin/bash
nohup streamlit run red.py &
root@6c3b593499bd:/app/colors# ps -ef |grep python
root         7     1  1 10:04 pts/0    00:00:13 /usr/bin/python3 /usr/local/bin/streamlit run red.py
root        45    36  0 10:23 pts/1    00:00:00 grep --color=auto python
root@6c3b593499bd:/app/colors#



5) stop all running containers

docker stop $(docker ps -aq)


delete all containers
--------------------------
docker rm $(docker ps -aq)



5) remove all images
---------------------

docker rmi $(docker images -a -q)