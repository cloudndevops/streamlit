#!/bin/bash
docker build --target app1 -t blue . 
docker build --target app2 -t red .
docker build --target app3 -t yellow .
docker build --target app4 -t pink .
nohup docker run -p 172.31.22.178:8501:8501 -t blue &
nohup docker run -p 172.31.22.178:8502:8501 -t red &
nohup docker run -p 172.31.22.178:8503:8501 -t yellow &
nohup docker run -p 172.31.22.178:8504:8501 -t pink &
echo "container started"
