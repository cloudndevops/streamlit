#color.py
import blue
import red
import green
import pink
import yellow
import streamlit as st
PAGES = {
    "Blue": blue,
    "Red": red,
    "Green": green,
    "Pink": pink,
    "Yellow": yellow
}
st.sidebar.title('Navigation')
selection = st.sidebar.radio("Go to", list(PAGES.keys()))
page = PAGES[selection]
page.local_css(file_name())
